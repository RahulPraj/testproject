from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from .forms import StaffRegisterForm,StudentRegisterForm,UserUpdateForm,ProfileUpdateForm
from django.contrib.auth.models import User
# Create your views here.


def staff_register(request):
    if request.method == 'POST':
        form = StaffRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('login')
    else:
        form = StaffRegisterForm()
    return render(request, 'user/register.html', {'form': form})


def student_register(request):
    if request.method == 'POST':
        form = StudentRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('login')
    else:
        form = StudentRegisterForm()
    return render(request, 'user/register.html', {'form': form})



def home(request):
   return render(request,'user/home.html')

def about(request):
    return render(request,'user/about.html')

@login_required
def profile(request):
    return render(request,'user/profile.html')

@login_required
def profileupdate(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid:
            u_form.save()
            p_form.save()
            messages.success(request, f'Account hasbeen Updated')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)       

    context = {
        'u_form' : u_form,
        'p_form' : p_form
    }
    return render(request,'user/profile.html', context)

@login_required
@staff_member_required
def staff_dash(request):
	users = User.objects.filter(is_staff=False)
	context = {
	'users' : users
	}
	return render(request, 'user/staffdash.html', context)

